package main

import (
	"encoding/json"
	"io"
	"math/rand"
	"os"
	"reflect"
	"time"
)

func getTargetFromJson(path string) (*JsonRes, error) {
	var data *JsonRes

	jsonFile, err := os.Open(path)
	if err != nil {
		return nil, err
	}
	defer jsonFile.Close()

	jsonValue, err := io.ReadAll(jsonFile)
	if err != nil {
		return nil, err
	}

	err = json.Unmarshal(jsonValue, &data)
	if err != nil {
		return nil, err
	}

	return data, nil
}

func InSlice(a int, list []int) bool {
	for _, b := range list {
		if b == a {
			return true
		}
	}
	return false
}

func generateRandomArray(l int) []int {
	rand.Seed(int64(time.Now().UTC().UnixNano()))

	result := []int{}

	for len(result) != l {
		num := rand.Intn(80) + 1
		if !InSlice(num, result) {
			result = append(result, num)
		}
	}

	return result
}

func removeDuplicateValues(intSlice []int) []int {
	keys := make(map[int]bool)
	list := []int{}

	for _, entry := range intSlice {
		if _, value := keys[entry]; !value {
			keys[entry] = true
			list = append(list, entry)
		}
	}
	return list
}

func isPretendentInSlice(pretendent Organism, target []Organism) bool {
	for _, t := range target {
		if reflect.DeepEqual(t.DNA, pretendent.DNA) {
			return true
		}
	}

	return false
}

func playTournament(organisms []Organism) Organism {
	var tournament []Organism

	for i := 0; i < TOURNAMENT_SIZE; i++ {
		idx := rand.Intn(len(organisms))
		o := organisms[idx]
		tournament = append(tournament, o)
	}

	var bestOrganism Organism
	for _, o := range tournament {
		if o.Fitness > bestOrganism.Fitness {
			bestOrganism = o
		}
	}

	return bestOrganism
}

func crossover(d1, d2 Organism) Organism {
	child := Organism{
		DNA:     make([]int, len(d1.DNA)),
		Fitness: 0,
	}

	var allDna []int
	allDna = append(allDna, d1.DNA...)
	allDna = append(allDna, d2.DNA...)

	allDnaWithoutDublicates := removeDuplicateValues(allDna)

	rand.Shuffle(len(allDnaWithoutDublicates), func(i, j int) {
		allDnaWithoutDublicates[i], allDnaWithoutDublicates[j] = allDnaWithoutDublicates[j], allDnaWithoutDublicates[i]
	})

	var result []int
	result = append(result, allDnaWithoutDublicates...)

	if len(result) > 10 {
		for len(result) != 10 {
			inx := rand.Intn(len(result))
			result = append(result[:inx], result[inx+1:]...)
		}
	} else if len(result) < 10 {
		for len(result) != 10 {
			num := rand.Intn(80) + 1
			for InSlice(num, result) {
				num = rand.Intn(80) + 1
			}
			result = append(result, num)
		}
	}

	child.DNA = result
	return child
}
