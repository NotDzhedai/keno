import pandas as pd
import numpy as np
import json

data = pd.read_csv("keno.csv", encoding="cp1251", sep=';')

data_set = pd.DataFrame(data=data)
data_set.drop(data_set.iloc[:, 24:], axis=1, inplace=True)
data_set_by_kulka = data_set.drop(["Розіграш", "Дата проведення", "Лототрон", "Комплект кульок"], axis=1)
 

result = []
for index, row in data_set_by_kulka.iterrows():
  result.append(list(row))


with open("data.json", "w") as f:
  json.dump({"data": result}, f)