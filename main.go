package main

import (
	"fmt"
	"log"
	"math/rand"
)

const (
	POPULATION_SIZE = 1000
	MUTATION_RATE   = 0.1
	SELECTION_SIZE  = 500
	CHILD_SIZE      = 250
	TOURNAMENT_SIZE = 10
)

type JsonRes struct {
	Data [][]int `json:"data"`
}

type Organism struct {
	DNA     []int
	Fitness int
	CashWin int
}

type Population struct {
	Organisms []Organism
	Target    [][]int
}

func (o *Organism) playGame(game []int) (int, int) {
	winCount := 0

	for i := 0; i < len(game); i++ {
		for j := 0; j < len(o.DNA); j++ {
			if o.DNA[j] == game[i] {
				winCount++
				break
			}
		}
	}

	var money int
	var score int

	switch winCount {
	case 0, 5:
		money = 20
		score = 1
	case 6:
		money = 100
		score = 1
	case 7:
		money = 500
		score = 1
	case 8:
		money = 5000
		score = 1
	case 9:
		money = 50000
		score = 1
	case 10:
		money = 1000000
		score = 1
	default:
		money = 0
		score = 0
	}

	return score, money
}

func (o *Organism) evaluate(target [][]int) {
	var score int
	var money int

	for _, game := range target {
		s, m := o.playGame(game)
		score += s
		money += m
	}

	o.CashWin = money
	o.Fitness = score
}

func createOrganism(target [][]int) Organism {
	o := Organism{
		DNA:     generateRandomArray(10),
		Fitness: 0,
	}

	o.evaluate(target)
	return o
}

func createPopulation(target [][]int) Population {
	organisms := make([]Organism, POPULATION_SIZE)
	for i := 0; i < POPULATION_SIZE; i++ {
		organisms[i] = createOrganism(target)
	}

	return Population{
		Target:    target,
		Organisms: organisms,
	}
}

func (p *Population) naturalSelection() []Organism {
	var organismsForCrossing []Organism

	for len(organismsForCrossing) != SELECTION_SIZE {
		o := playTournament(p.Organisms)

		// check if this item already in slice or not
		if !isPretendentInSlice(o, organismsForCrossing) {
			organismsForCrossing = append(organismsForCrossing, o)
		}
	}

	return organismsForCrossing
}

func (p *Population) naturalCrossing(organisms []Organism) {
	for i := 0; i < CHILD_SIZE; i++ {
		r1, r2 := rand.Intn(len(organisms)), rand.Intn(len(organisms))
		a, b := organisms[r1], organisms[r2]

		child := crossover(a, b)
		child.mutate()
		child.evaluate(p.Target)
		p.Organisms = append(p.Organisms, child)
	}
}

func (p *Population) removeByIndex(index int) {
	p.Organisms = append(p.Organisms[:index], p.Organisms[index+1:]...)
}

func (p *Population) kill() {
	for i := 0; i < CHILD_SIZE; i++ {
		idx := rand.Intn(len(p.Organisms))
		p.removeByIndex(idx)
	}
}

func (o *Organism) mutate() {
	for i := 0; i < len(o.DNA); i++ {
		if rand.Float64() < MUTATION_RATE {
			num := rand.Intn(80) + 1
			for InSlice(num, o.DNA) {
				num = rand.Intn(80) + 1
			}
			o.DNA[i] = num
		}
	}
}

func (p *Population) getBest() Organism {
	best := 0
	index := 0
	for i := 0; i < len(p.Organisms); i++ {
		if p.Organisms[i].Fitness > best {
			index = i
			best = p.Organisms[i].Fitness
		}
	}

	return p.Organisms[index]
}

func main() {
	data, err := getTargetFromJson("data.json")
	if err != nil {
		log.Fatal(err)
	}

	target := data.Data

	population := createPopulation(target)

	found := false
	generationCount := 0

	var bestForAllGenerations Organism

	log.Println("START FOR TARGET WITH LEN: ", len(target))
	for !found {
		generationCount++

		bestOnThisGeneration := population.getBest()
		if bestOnThisGeneration.Fitness > bestForAllGenerations.Fitness {
			bestForAllGenerations = bestOnThisGeneration
		}

		fmt.Println("********************************")
		fmt.Println("** Generation: ", generationCount)
		fmt.Println("** Size of population: ", len(population.Organisms))
		fmt.Println("********************************")
		fmt.Println("** Best DNA of THIS generation: ", bestOnThisGeneration.DNA)
		fmt.Println("** Best FITNESS of THIS generation: ", bestOnThisGeneration.Fitness)
		fmt.Println("** Best CASH WIN of THIS generation: ", bestOnThisGeneration.CashWin)
		fmt.Println("********************************")
		fmt.Println("** Best DNA of ALL generation: ", bestForAllGenerations.DNA)
		fmt.Println("** Best FITNESS of ALL generation: ", bestForAllGenerations.Fitness)
		fmt.Println("** Best CASH WIN of ALL generation: ", bestForAllGenerations.CashWin)

		crossOrgs := population.naturalSelection()
		population.naturalCrossing(crossOrgs)
		population.kill()
	}
}
